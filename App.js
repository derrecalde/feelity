import React from "react";
import { NavigationContainer } from "@react-navigation/native";

import Root from "./app/Root";

export default function Navigation() {
  return (
    <NavigationContainer>
      <Root />
    </NavigationContainer>
  );
}