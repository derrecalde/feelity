import React from 'react'
import { StyleSheet, View, useWindowDimensions } from 'react-native'

export default function DefaultBackground() {
  const window = useWindowDimensions();
  const landscape = Math.round(window.height) < 600 ? true : false;

  return (
    <View style={[styles.parentContainer, { height: Math.round(window.height) }] } >
      <View style={{flex: landscape ? 0.5 : 0.3, backgroundColor: '#222222' }} />
    </View>
  )
}

const styles = StyleSheet.create({
  parentContainer:{
    flex: 1,
    position: 'absolute',
    width: '100%'
  }
})
