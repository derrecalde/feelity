import React from 'react'
import { StyleSheet, Text, View, Image, SafeAreaView, TouchableOpacity, ScrollView } from 'react-native'
import {useRoute} from '@react-navigation/native';
import Header from '../Header';
import CardSilder from 'react-native-cards-slider';

export default function ServiceScreen({ navigation }) {
  const goTo = () => navigation.navigate("Home");
  const route = useRoute();
  
  return (
    <View>
      <ScrollView>
      <Header route={route.name}/>
      <CustomGobackButton onPress={goTo} />
      <SafeAreaView>
        <View style={styles.sliderContainer}>
          <CardSilder>
              <Image style={styles.imageSlider} source={{
                width:300,
                height: 300,
                uri : 'https://picsum.photos/300/300?hmac=1'}} />
              <Image style={styles.imageSlider} source={{
                width:300,
                height: 300,
                uri : 'https://picsum.photos/300/300?hmac=2'}} />
              <Image style={styles.imageSlider} source={{
                width:300,
                height: 300,
                uri : 'https://picsum.photos/300/300?hmac=3'}} />
          </CardSilder>
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.title} >Articles</Text>
          <Text style={styles.text} >Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</Text>
          <CustomButton onPress={goTo} title={`Project`} />
        </View>
      </SafeAreaView>
      </ScrollView>
    </View>
  )
}

const CustomGobackButton = ({onPress}) => (
  <TouchableOpacity onPress={onPress} style={styles.customGoBackButtonContainer} >
    <Text style={styles.customGoBackItemButton}>{"<"}</Text>
  </TouchableOpacity>
);

const CustomButton = ({ onPress, title }) => (
  <TouchableOpacity onPress={onPress} style={styles.customButtonContainer} >
    <Text style={styles.customButtonText}>{title}</Text>
    <Text style={styles.customItemButton}>{">"}</Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  sliderContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: 300,
    marginTop: 20
  },
  customGoBackButtonContainer:{
    backgroundColor: 'grey',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 15,
    position: 'absolute',
    top: 10,
    left: 20,
    zIndex: 1,
  },
  customGoBackItemButton:{
    fontSize: 30,
    color: 'white',
  },
  imageSlider:{
    width: '100%',
    height: 300,
    borderRadius: 20,
  },
  textContainer:{
    padding: 20
  },
  title:{
    fontSize: 50,
    fontWeight: 'bold',
    color: '#222222',
    paddingBottom: 10,
  },
  text:{
    fontSize: 20,
    fontWeight: 'bold',
    color: '#222222',
    paddingBottom: 10,
  },
  customButtonContainer: {
    backgroundColor: '#615f5e3b',
    borderRadius: 15,
    paddingHorizontal: 20,
    elevation: 0,
    paddingVertical: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  customButtonText:{
    fontSize: 18,
    color: '#222222',
    fontWeight: 'bold',
    alignSelf: "center",
    textTransform: "uppercase"
  },
  customItemButton:{
    fontSize: 30,
    color: '#222222',
    backgroundColor: 'white',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 20,
  }
})
