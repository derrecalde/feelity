import React, { useRef, useEffect } from 'react'
import {Animated, StyleSheet, Text, View, ScrollView, SafeAreaView, TouchableOpacity, useWindowDimensions } from 'react-native'
import Header from '../Header';
import {useRoute} from '@react-navigation/native';


export default function HomeScreen({ navigation }) {
  const goTo = () => navigation.navigate("Service");
  const route = useRoute();
  const window = useWindowDimensions();
  const landscape = Math.round(window.height) < 600 ? true : false;

  return (
    <View>
      <ScrollView>
        <Header route={route.name}/>
        <SafeAreaView style={[styles.container, {paddingTop: landscape ? '10%' : '30%'}]}>
          <FadeInView>
            <Text style={styles.baseText}>Test</Text>
          </FadeInView>
          <FadeInView duration="1000">
            <Text style={styles.baseText}>ReactNative</Text>
          </FadeInView>
          <FadeInView duration="1500" style={{ width: '50%', paddingTop: 40 }}>
            <CustomButton onPress={goTo} title={`Start`} />
          </FadeInView>
        </SafeAreaView>
      </ScrollView>
    </View>
  )
}

const CustomButton = ({ onPress, title }) => (
  <TouchableOpacity onPress={onPress} style={styles.customButtonContainer} >
    <Text style={styles.customButtonText}>{title}</Text>
    <Text style={styles.customItemButton}>{">"}</Text>
  </TouchableOpacity>
);

const FadeInView = (props) => {
  const fadeAnim = useRef(new Animated.Value(0)).current  // Initial value for opacity: 0

  React.useEffect(() => {
    Animated.timing(
      fadeAnim,
      {
        toValue: 1,
        duration: props.duration ? Math.round(props.duration) : 500,
        useNativeDriver: true
      }
    ).start();
  }, [fadeAnim])

  return (
    <Animated.View                 // Special animatable View
      style={{
        ...props.style,
        opacity: fadeAnim,         // Bind opacity to animated value
      }}
    >
      {props.children}
    </Animated.View>
  );
}

const styles = StyleSheet.create({
  container:{
    paddingTop:'30%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
  },
  baseText:{
    fontSize: 38, 
    paddingTop: 20,
    textAlign: 'center', 
    margin: 10,
    color : 'grey',
  },
  btnContainer:{
    backgroundColor: 'gray',
    color: 'gray',
  },
  customButtonContainer: {
    backgroundColor: '#615f5e3b',
    borderRadius: 15,
    paddingHorizontal: 20,
    elevation: 0,
    paddingVertical: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  customButtonText:{
    fontSize: 18,
    color: '#222222',
    fontWeight: 'bold',
    alignSelf: "center",
    textTransform: "uppercase"
  },
  customItemButton:{
    fontSize: 30,
    color: '#222222',
    backgroundColor: 'white',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 20,
  }
})
