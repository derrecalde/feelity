import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import Home from "./screens/HomeScreen";
import Service from "./screens/ServiceScreen";
import Header from "./Header";

const Stack = createStackNavigator();

export default function Root() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Service" component={Service} />
      <Stack.Screen name="Header" component={Header} />
    </Stack.Navigator>
  );
}