import React from 'react'
import { StyleSheet, Text, View, SafeAreaView } from 'react-native'
import DefaultBackground from './DefaultBackground';
import {useRoute} from '@react-navigation/native';

export default function Header({route}) {

  return (
    <View>
      <DefaultBackground/>
      <SafeAreaView>
        <Text style={styles.headerContainer}>
          <Text style={[styles.headerContainer, {fontWeight: 'bold'}]}>{"<><>"} </Text>Feelity
        </Text>
        <Text style={styles.titleContainer} >{route}</Text>
      </SafeAreaView>
    </View>
  )
}

const styles = StyleSheet.create({
  headerContainer: {
    color: 'white',
    fontSize: 30,
    fontFamily: 'Roboto',
    textAlign: 'center',
  },
  titleContainer:{
    fontSize: 60,
    color: 'white',
    fontWeight: 'bold',
    paddingLeft: 20,
    paddingTop: 40,
  }
})
