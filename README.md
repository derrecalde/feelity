# Feelity

Test based on React Native.   
This test required an emulator smartphone android or ios (ex : Android Studio).   

## Getting started
* Install packages   
```yarn -i``` or ```npm install```  

* Run application on your emulator   
```expo start``` or ```expo start --android``` (if need to specify)   

